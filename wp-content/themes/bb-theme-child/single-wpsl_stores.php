<?php get_header(); ?>

<?php
	global $post;

	$queried_object = get_queried_object();

		$address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
		$city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
		$zip          = get_post_meta( $queried_object->ID, 'wpsl_zip', true );
		$state          = get_post_meta( $queried_object->ID, 'wpsl_state', true );
		$phone       = get_post_meta( $queried_object->ID, 'wpsl_phone', true );
		$site_url       = get_post_meta( $queried_object->ID, 'wpsl_site_url', true );
		$url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
		$destination   = $address . ',' . $city . ',' . $state.',' .$zip;
		$direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";

	?>

<div class="container fl-content-full">



	<div class="row fl-row fl-row-full-width">	

	

	<img src="https://bigbobsflooring-stg.mm-dev.agency/wp-content/uploads/2021/11/BigBobsLogo.png"/>
		
	<h1 class="entry-title sfnstoretitle">BIG BOB'S <?php echo $state;  ?></h1>
	<span>AUTORIZED BIG BOB'S DEALER</span>   
	<span><a href="htpps://<?php echo $site_url;?>" target="_blank"><?php echo $site_url;  ?></a></span>  

	<span>PHONE NUMBER </span>
	<span><?php echo $phone;?> </span>

	<div class="storehours">
	<span>HOURS </span>
	<?php echo do_shortcode( '[wpsl_hours]' ); ?>
	</div>

	<span>BIG BOB'S SERVICES </span>
        <ul class="store_services">
	     
		  <li><a href="#" class="getdirect"> SHOP AT HOME</a></li>
		  <li><a href="#" class="getdirect"> BUY RUGS ONLINE</a></li>
		  <li><a href="#" class="button"> SCHEDULE A MEASURE</a></li>
		 
		</ul>

		<ul class="product_offered">
	     
		  <li><a href="#" class="getdirect">CARPET</a></li>
		  <li><a href="#" class="getdirect"> HARDWOOD</a></li>
		  <li><a href="#" class="button"> VINYL</a></li>
		  <li><a href="#" class="getdirect"> LAMINATE</a></li>
		  <li><a href="#" class="getdirect"> TILES</a></li>
		  <li><a href="#" class="button"> RUGS ONLINE</a></li>
		 
		</ul>
         
		
    </div>

	<div class="mapcontainer">

		<h2 class="entry-title sfnstoretitle">BIG BOB'S <?php echo $state;  ?></h2>
<span><?php echo $destination;?></span>
		<a href="<?php echo $direction_url ;?>">GET DIRECTIONS</a>
			<?php echo do_shortcode( '[wpsl_map]' ); ?>
			
		
	

	</div>
</div>

<?php get_footer(); ?>
